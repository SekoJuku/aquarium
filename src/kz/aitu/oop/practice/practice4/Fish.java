package kz.aitu.oop.practice.practice4;

public interface Fish {
    String sleep(); // sleeps any time
    String swim(); // swims
    String play(); // don't know why?
    String eat(String meal); // fish should eat something,no?
}
