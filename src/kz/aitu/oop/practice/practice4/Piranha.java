package kz.aitu.oop.practice.practice4;

public class Piranha implements Fish {
    private String name;

    public Piranha(String name) {
        setname(name);
    }

    private void setname(String name) {
        this.name = name;
    }

    public String getname() {
        return name;
    }

    @Override
    public String sleep() {

        return "Fish slept some hours";
    }

    @Override
    public String swim() {

        return "Fish swam";
    }

    @Override
    public String play() {

        return "Fish .. played";
    }
    @Override
    public String eat(String meal) {
        if(meal == "meat") {
            return "He ate\n";
        }
        else {
            return "He burped\n";
        }
    }
}
