package kz.aitu.oop.practice.practice4;

import java.util.ArrayList;
import java.util.Scanner;

public class Aquarium {
    private int capacity;
    private String name;
    private int fishnum;

    public ArrayList<Aguaruna> aguarunalist  = new ArrayList<>();
    public ArrayList<Piranha> piranhalist = new ArrayList<>();
    public ArrayList<GoldFish> goldfishlist  = new ArrayList<>();


    public Aquarium(String name,int capacity) {
        setName(name);
        setCapacity(capacity);
        fishnum = 0;
    }

    public String addfish() {
        if(fishnum >= getCapacity()) return "You can't add more fish\n";
        Scanner sc = new Scanner(System.in);
        System.out.println("1. Aguaruna" + "\n" +
                           "2. Piranha" + "\n" +
                           "3. GoldFish" + "\n");
        int choice = sc.nextInt();
        System.out.println("Name your fish:");
        String name = sc.next();
        if(choice == 1) {
            Aguaruna a = new Aguaruna(name);
            aguarunalist.add(a);
        }
        if(choice == 2) {
            Piranha a = new Piranha(name);
            piranhalist.add(a);
        }
        if(choice == 3) {
            GoldFish a = new GoldFish(name);
            goldfishlist.add(a);
        }
        fishnum++;
        String s = "Fish added successfully\n";
        return s;

    }

    private void setName(String name) {
        this.name = name;
    }

    private void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getName() { return name; }
    public int getCapacity() { return capacity; }
    public int getFishnum() { return fishnum; }


}
