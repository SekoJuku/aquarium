package kz.aitu.oop.practice.practice4;

public class GoldFish implements Fish {
    private String name;

    public GoldFish(String name) {
        setname(name);
    }

    private void setname(String name) {
        this.name = name;
    }

    public String getname() {
        return name;
    }

    @Override
    public String sleep() {

        return "Fish slept some hours";
    }

    @Override
    public String swim() {

        return "Fish swam";
    }

    @Override
    public String play() {

        return "Fish .. played";
    }
    @Override
    public String eat(String meal) {
        return "He ate\n"; // He eats everything
    }
}
